const items = [
    { name: 'Bike',     price: 100 },
    { name: 'TV',     price: 200 },
    { name: 'Album',     price: 10 },
    { name: 'Book',     price: 5 },
    { name: 'Phone',     price: 500 },
    { name: 'Computer',     price: 1000 },
    { name: 'Keyboard',     price: 25 },
]

//Filter 
const filteredItems = items.filter((item) => {
    return item.price <= 100;
});
console.log("filter: \n" + filteredItems.toString());

//Map
const itemNames = items.map((item)=> {
    return item.name;
});
console.log("map: \n" + itemNames);

//Find
const foundItem = items.find((item)=> {
    return item.name === 'Book';
});
console.log("find: \n" + foundItem);

//ForEach
items.forEach((item)=> {
    console.log(item.name + " costs " + item.price);
});

//Some
//return true/false
const hasInexpensiveItems = items.some((item)=> {
    return item.price <= 100;
});
console.log(hasInexpensiveItems);

//Every
//Checks ALL items
const hasFreeItems = items.every((item)=> {
    return item.price <= 0;
});
console.log(hasFreeItems);

//Reduce
const total = items.reduce((currentTotal, item) => {
return item.price + currentTotal;
}, 0);
console.log(total);

//Includes
const numbers = [0, 2, 5, 3, 666];
const includesTwo = numbers.includes(2);
console.log(includesTwo);